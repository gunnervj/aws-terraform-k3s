# ---------- compute/main.tf ----------------#
resource "random_shuffle" "public_subnets" {
  input        = var.public_subnets
  result_count = var.instance_count
}

data "aws_ami" "ec2_ami" {
  most_recent = true
  owners      = var.aws_ami_owners

  filter {
    name   = "name"
    values = [var.aws_ami_filter_name]
  }
}

resource "random_id" "ec2-name-generator" {
  byte_length = 2
  count       = var.instance_count
  keepers = {
    key_name = var.key_name
  }
}

resource "aws_instance" "my-ec2-node" {
  count                  = var.instance_count
  ami                    = data.aws_ami.ec2_ami.id
  instance_type          = var.instance_type
  vpc_security_group_ids = var.public_sg
  subnet_id              = random_shuffle.public_subnets.result[count.index]
  key_name               = aws_key_pair.ec2-auth.id
  user_data = base64encode(templatefile(var.user_data_template_path, {
    nodename    = "${var.project}-node-${random_id.ec2-name-generator[count.index].dec}"
    db_endpoint = var.db_endpoint
    dbuser      = var.db_username
    dbpass      = var.db_password
    dbname      = var.db_name
  }))

  root_block_device {
    volume_size = var.ec2_root_vol_size
  }

  tags = {
    "Name" = "${var.project}-node-${random_id.ec2-name-generator[count.index].dec}"
  }

}

resource "aws_key_pair" "ec2-auth" {
  key_name   = var.key_name
  public_key = file(var.public_key_path)
}

resource "aws_lb_target_group_attachment" "ec2-nodes-tg-attachment" {
  count            = var.instance_count
  target_group_arn = var.lb_target_group_arn
  target_id        = aws_instance.my-ec2-node[count.index].id
  port             = 8000
}