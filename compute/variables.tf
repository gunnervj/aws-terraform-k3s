# ---------- compute/variables.tf ----------------#
variable "project" {
  type        = string
  default     = "aws-terraform"
  description = "Name of the Project for which the resources are being created."
}

variable "aws_ami_owners" {
  type        = list(string)
  default     = ["099720109477"]
  description = "AWS AMI Owner"
}

variable "aws_ami_filter_name" {
  type        = string
  default     = "ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"
  description = "AWS AMI Name"
}

variable "instance_count" {
  type        = number
  default     = 2
  description = "Number of Nodes(EC2) instances"
}

variable "instance_type" {
  type        = string
  default     = "t2.micro"
  description = "EC2 instance type for node."
}

variable "public_sg" {
  type        = list(string)
  description = "Public Security Group"
}

variable "ec2_root_vol_size" {
  type        = number
  default     = 10
  description = "Ec2 Root Volume Size"
}

variable "public_subnets" {
  type        = list(string)
  description = "List of Subnets to launch EC2 instances"
}

variable "key_name" {
  type        = string
  description = "EC2 keypair name which can be used to login to EC2 Nodes."
}

variable "public_key_path" {
  type        = string
  description = "Location of EC2 login public keypair"
}

variable "user_data_template_path" {
  type        = string
  description = "User Data template file location"
}

variable "db_endpoint" {
  type        = string
  description = "DB endpoint string of K3S backend external database"
}

variable "db_name" {
  type        = string
  description = "DB name."
}
variable "db_username" {
  type        = string
  description = "Database username"
  sensitive   = true
}
variable "db_password" {
  type        = string
  description = "Database Password"
  sensitive   = true
}

variable "lb_target_group_arn" {
  type        = string
  description = "Loadbalancer target group arn."
}