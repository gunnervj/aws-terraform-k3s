variable "project" {
  type        = string
  default     = "aws-terraform"
  description = "Name of the Project for which the resources are being created."
}
variable "additional_default_tags" {
  type        = map(string)
  description = "These will be the default tags for the resources."
}

variable "created_by" {
  type        = string
  description = "Will be added as a tag in the resources."
}

variable "region" {
  type        = string
  description = "AWS Region where the resource creation has to happen."
  default     = "ap-south-1"
}

############# NETWORK VARIBLES ################

variable "vpc_cidr" {
  type        = string
  description = "CIDR block for the VPC"
}

variable "public_subnet_count" {
  type    = number
  default = 2
  validation {
    condition     = var.public_subnet_count <= 3
    error_message = "Maximum Allowed Public Subnet Count is 3."
  }
}

variable "private_subnet_count" {
  type    = number
  default = 2
  validation {
    condition     = var.private_subnet_count >= 2 && var.private_subnet_count <= 3
    error_message = "Maximum Allowed Private Subnet Count is 3 and Minimum number of Private Subnets is 2."
  }
}

variable "access_ip_public_ssh" {
  type = string
}

############# DB VARIBLES ################

variable "db_engine_version" {
  type        = string
  description = "DB Engine version"
}

variable "db_instance_class" {
  type        = string
  default     = "db.t2.micro"
  description = "Database instance class"
}

variable "db_name" {
  type        = string
  description = "Database Name."
}

variable "db_password" {
  type        = string
  sensitive   = true
  description = "Database Password"
}

variable "db_username" {
  type        = string
  sensitive   = true
  description = "Database username"
}

variable "db_identifier" {
  type        = string
  description = "Database Identifier"
}

variable "skip_final_snapshot" {
  type        = bool
  default     = true
  description = "True to skip final snapshot."
}

variable "db_storage" {
  type        = number
  default     = 10
  description = "DB Storage number"
}

variable "db_engine" {
  type        = string
  default     = "mysql"
  description = "DB Engine"
}

############# LOADBALANCER VARIBLES ################

variable "tg_port" {
  type    = string
  default = "80"
}
variable "tg_proto" {
  type    = string
  default = "HTTP"
}
variable "alb_path" {
  type    = string
  default = "/"
}
variable "alb_healthy_threshold" {
  type    = number
  default = 3
}
variable "alb_unhealthy_threshold" {
  type    = number
  default = 3
}
variable "alb_timeout" {
  type    = number
  default = 30
}
variable "alb_interval" {
  type    = number
  default = 60
}
variable "alb_proto" {
  type    = string
  default = "HTTP"
}

############# COMPUTE VARIBLES ################

variable "node_instance_count" {
  type        = number
  default     = 2
  description = "Number of EC2 Nodes"
}
variable "node_instance_type" {
  type        = string
  default     = "t2.micro"
  description = "EC2 node instance type."
}

variable "node_root_vol_size" {
  type        = number
  default     = 10
  description = "Default Instance Vol Size"
}

variable "key_name" {
  type        = string
  description = "EC2 keypair name which can be used to login to EC2 Nodes."
}

variable "public_key_path" {
  type        = string
  description = "Location of EC2 login public keypair"
}

variable "aws_ami_filter_name" {
  type        = string
  default     = "ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-*"
  description = "Can be used to filter the AMI based on name"
}

variable "aws_ami_owners" {
  type        = list(string)
  default     = ["099720109477"]
  description = "AWS AMI owner."
}