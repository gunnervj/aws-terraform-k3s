terraform {
  cloud {
    organization = "thebitbytebox"

    workspaces {
      name = "bbb-dev"
    }
  }
}