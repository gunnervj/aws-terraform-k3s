# Terraform - AWS K3S

This project creates an environment to deploy k3S kubernetes cluster backed by an RDS Server. K3S servers are created in the public ssubnet to avoid costs of having a NAT gateway in private subnet and to remain in free tier. Also we would require a jump box in public subnet to login into the servers in private subnet.

### Architecture

![Architecture](images/architecture.png)

### Networking Module
1. Creates a VPC with public and private subnets. 
2. Create a public route table and use default route table as private.
3. Creates a security groups for public and private subnets.

### Load Balancer Module
1. Creates an ALB.
2. Creates a target group
2. Creates an ALB Listerner on Port 80.

### Compute Module
1. Creates EC2 instances for K3S cluster nodes.
2. Executes userdata on EC2 instances to download and run K3S.
3. Attaches the instances to the ALB target group.

Backend for the terraform is configured with terraform  cloud. So you would need to create an account and login to it. Once logged in, you can run terraform init, plan and deploy commands. 

**Note**: You must have configured AWS CLI with access key and token to deploy from local.

## Deploy Nginx Server

To deploy Nginx server in K3S
1. Login to the ec2 instance with the ssh private key.
2. Copy deplyment.yaml to EC2 server.
3. Run kubectl apply -f nginx-deployment.yaml

Once done, you should be able to access the Nginx webserver via the ALB endpoint.