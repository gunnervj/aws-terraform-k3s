locals {
  common_tags = {
    "created_by"        = var.created_by
    "created_by_script" = "aws-terraform"
    "project"           = var.project
  }
}

locals {
  security_group_public = {
    name        = "my-public-sg"
    description = "My Public Security Group"
    ingress = {
      ssh = {
        from        = 22
        to          = 22
        protocol    = "tcp"
        cidr_blocks = [var.access_ip_public_ssh]
      }
      http = {
        from        = 80
        to          = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }
      nginx = {
        from        = 8000
        to          = 8000
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
      }
    }
  }
}

locals {
  security_groups = {
    public_alb = {
      name        = "my-public-alb-sg"
      description = "My Public ALB Security Group"
      ingress = {
        http = {
          from        = 80
          to          = 80
          protocol    = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
    rds = {
      name        = "rds_sg"
      description = "my-rds-sg"
      ingress = {
        mysql = {
          from        = 3306
          to          = 3306
          protocol    = "tcp"
          cidr_blocks = [var.vpc_cidr]
        }
      }
    }
  }
}