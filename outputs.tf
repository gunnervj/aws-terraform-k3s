output "loadbalancer_endpoint" {
  value = module.loadbalancer.alb_endpoint
}

output "instances_ip" {
  value = module.my-ec2-nodes.instances_ip
}