#------- database/variable.tf ------------------ #

variable "db_engine" {
  type        = string
  default     = "mysql"
  description = "DB Engine"
}

variable "db_storage" {
  type        = number
  default     = 10
  description = "DB Storage number"
}
variable "db_engine_version" {
  type        = string
  description = "DB Engine version"
}
variable "db_instance_class" {
  type        = string
  default     = "db.t2.small"
  description = "DB instance class"
}
variable "db_name" {
  type        = string
  description = "DB name."
}
variable "db_username" {
  type        = string
  description = "Database username"
  sensitive   = true
}
variable "db_password" {
  type        = string
  description = "Database Password"
  sensitive   = true
}
variable "db_subnet_group_name" {
  type        = string
  description = "Database subnet group name"
}
variable "vpc_security_group_ids" {
  type        = list(string)
  description = "VPC RDS Private Security Group Ids"
}
variable "db_identifier" {
  type        = string
  description = "Database Identifier"
}
variable "skip_final_snapshot" {
  type        = bool
  description = "True to skip final snapshot."
}