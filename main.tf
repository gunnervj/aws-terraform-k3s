# ------- root/main.tf --------#

module "networking" {
  source                = "./networking"
  vpc_cidr              = var.vpc_cidr
  project               = var.project
  private_subnet_count  = var.private_subnet_count
  public_subnet_count   = var.public_subnet_count
  access_ip_public_ssh  = var.access_ip_public_ssh
  security_groups       = local.security_groups
  security_group_public = local.security_group_public
  db_subnet_group       = var.private_subnet_count > 1 ? true : false
}

module "database" {
  source                 = "./database"
  db_storage             = var.db_storage
  db_engine              = var.db_engine
  db_engine_version      = var.db_engine_version
  db_instance_class      = var.db_instance_class
  db_name                = var.db_name
  db_username            = var.db_username
  db_password            = var.db_password
  db_identifier          = var.db_identifier
  skip_final_snapshot    = var.skip_final_snapshot
  db_subnet_group_name   = module.networking.my_rds_subnetgroup_name[0]
  vpc_security_group_ids = module.networking.my_rds_security_group
}

module "loadbalancer" {
  source                               = "./loadbalancer"
  public_sg                            = module.networking.public_alb_sg
  public_subnets                       = module.networking.public_subnets
  alb_health_check_path                = var.alb_path
  alb_health_check_healthy_threshold   = var.alb_healthy_threshold
  alb_health_check_unhealthy_threshold = var.alb_unhealthy_threshold
  alb_health_check_timeout             = var.alb_timeout
  alb_health_check_interval            = var.alb_interval
  alb_health_check_proto               = var.alb_proto
  tg_port                              = var.tg_port
  tg_proto                             = var.tg_proto
  vpc_id                               = module.networking.vpc_id
}

module "my-ec2-nodes" {
  project                 = var.project
  source                  = "./compute"
  aws_ami_filter_name     = var.aws_ami_filter_name
  aws_ami_owners          = var.aws_ami_owners
  instance_count          = var.node_instance_count
  instance_type           = var.node_instance_type
  public_sg               = module.networking.public_sg
  public_subnets          = module.networking.public_subnets
  ec2_root_vol_size       = var.node_root_vol_size
  public_key_path         = var.public_key_path
  key_name                = var.key_name
  user_data_template_path = "${path.root}/userdata.tpl"
  db_endpoint             = module.database.db_endpoint
  db_name                 = var.db_name
  db_password             = var.db_password
  db_username             = var.db_username
  lb_target_group_arn     = module.loadbalancer.alb_target_grp_arn
}
