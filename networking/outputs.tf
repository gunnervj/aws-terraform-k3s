#---------- networking/outputs.tf
output "vpc_id" {
  value = aws_vpc.my-vpc.id
}

output "my_rds_subnetgroup_name" {
  value = aws_db_subnet_group.my-rds-subnetgroup.*.name
}

output "my_rds_security_group" {
  value = [aws_security_group.my-sg["rds"].id]
}

output "public_sg" {
  value = [aws_security_group.my-public-sg.id]
}

output "public_alb_sg" {
  value = [aws_security_group.my-sg["public_alb"].id]
}

output "public_subnets" {
  value = aws_subnet.my-public-subnet.*.id
}