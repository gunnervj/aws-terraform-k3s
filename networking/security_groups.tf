resource "aws_security_group" "my-sg" {
  for_each    = var.security_groups
  name_prefix = each.value.name
  description = each.value.description
  vpc_id      = aws_vpc.my-vpc.id

  dynamic "ingress" {
    for_each = each.value.ingress
    content {
      from_port   = ingress.value.from
      to_port     = ingress.value.to
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow All Outbound Traffic."
  }

}


resource "aws_security_group" "my-public-sg" {
  name_prefix = var.security_group_public.name
  description = var.security_group_public.description
  vpc_id      = aws_vpc.my-vpc.id

  dynamic "ingress" {
    for_each = var.security_group_public.ingress
    content {
      from_port       = ingress.value.from
      to_port         = ingress.value.to
      protocol        = ingress.value.protocol
      security_groups = [aws_security_group.my-sg["public_alb"].id]
    }
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
    description = "Allow All Outbound Traffic."
  }

}
