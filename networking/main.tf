
resource "random_shuffle" "az_list_public" {
  input        = data.aws_availability_zones.available.names
  result_count = 3
}

resource "random_shuffle" "az_list_private" {
  input        = data.aws_availability_zones.available.names
  result_count = 3
}

resource "aws_vpc" "my-vpc" {
  cidr_block                       = var.vpc_cidr
  assign_generated_ipv6_cidr_block = true
  instance_tenancy                 = "default"
  enable_dns_support               = true
  enable_dns_hostnames             = true

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    "Name" = "${var.project}-vpc"
  }
}

##################### PUBLIC ZONE ###########################

# internet gateway
resource "aws_internet_gateway" "my-internet-gateway" {
  vpc_id = aws_vpc.my-vpc.id

  tags = {
    "Name" = "${var.project}-igw"
  }
}

# Public Subnet
resource "aws_subnet" "my-public-subnet" {
  count                   = var.public_subnet_count
  vpc_id                  = aws_vpc.my-vpc.id
  cidr_block              = local.public_cidr_blocks[count.index]
  map_public_ip_on_launch = true
  availability_zone       = random_shuffle.az_list_public.result[count.index]

  tags = {
    "Name" = "${var.project}-public-${data.aws_availability_zones.available.names[count.index]}"
  }
}

# public route table and routes
resource "aws_route_table" "my-public-route-table" {
  vpc_id = aws_vpc.my-vpc.id

  tags = {
    "Name" = "${var.project}-public-rt"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my-internet-gateway.id
  }

}

resource "aws_route_table_association" "my-public-route-table-asso" {
  count          = var.public_subnet_count
  route_table_id = aws_route_table.my-public-route-table.id
  subnet_id      = element(aws_subnet.my-public-subnet.*.id, count.index)
}


##################### PRIVATE ZONE ###########################

# private subnet
resource "aws_subnet" "my-private-subnet" {
  count             = var.private_subnet_count
  vpc_id            = aws_vpc.my-vpc.id
  cidr_block        = local.private_cidr_blocks[count.index]
  availability_zone = random_shuffle.az_list_private.result[count.index]

  tags = {
    "Name" = "${var.project}-private-${data.aws_availability_zones.available.names[count.index]}"
  }
}

resource "aws_default_route_table" "my-private-route-table" {
  default_route_table_id = aws_vpc.my-vpc.default_route_table_id

  tags = {
    "Name" = "${var.project}-private-rt"
  }
}

resource "aws_route_table_association" "my-private-route-table-asso" {
  count          = var.private_subnet_count
  route_table_id = aws_default_route_table.my-private-route-table.id
  subnet_id      = element(aws_subnet.my-private-subnet[*].id, count.index)
}


resource "aws_db_subnet_group" "my-rds-subnetgroup" {
  count      = var.db_subnet_group ? 1 : 0
  name       = "my_rds_subnet_group"
  subnet_ids = aws_subnet.my-private-subnet.*.id
  tags = {
    Name = "my_rds_sng"
  }
}


