#----- Networking/main.tf-------
variable "project" {
  type        = string
  default     = "aws-terraform"
  description = "Name of the Project for which the resources are being created."
}

variable "vpc_cidr" {
  type        = string
  description = "CIDR block for the VPC"
}

variable "public_subnet_count" {
  type    = number
  default = 2
  validation {
    condition     = var.public_subnet_count <= 3
    error_message = "Exceeded the maximum allowed public subnet count."
  }
}

variable "private_subnet_count" {
  type    = number
  default = 1
  validation {
    condition     = var.private_subnet_count <= 3
    error_message = "Exceeded the maximum allowed private subnet count."
  }
}

variable "access_ip_public_ssh" {
  type = string
}

variable "security_groups" {
}

variable "security_group_public" {
}

variable "db_subnet_group" {
  type    = bool
  default = true
}

