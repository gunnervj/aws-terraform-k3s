# ------------- loadbalancer/main.tf ----------------#

resource "aws_lb" "my-alb" {
  name               = "${var.project}-alb"
  load_balancer_type = "application"
  security_groups    = var.public_sg
  subnets            = var.public_subnets
  idle_timeout       = 60

  tags = {
    "Name" = "${var.project}-alb"
  }
}

resource "aws_lb_target_group" "my-alb-client-tg" {
  name     = "${var.project}-alb-tg-${substr(uuid(), 0, 4)}"
  port     = var.tg_port
  protocol = var.tg_proto
  vpc_id   = var.vpc_id

  lifecycle {
    ignore_changes        = [name]
    create_before_destroy = true
  }

  health_check {
    enabled             = true
    path                = var.alb_health_check_path
    healthy_threshold   = var.alb_health_check_healthy_threshold
    unhealthy_threshold = var.alb_health_check_unhealthy_threshold
    timeout             = var.alb_health_check_timeout
    interval            = var.alb_health_check_interval
    protocol            = var.alb_health_check_proto
  }

  tags = {
    "Name" = "${var.project}-alb-tg"
  }
}

resource "aws_lb_listener" "my-alb-http_80" {
  load_balancer_arn = aws_lb.my-alb.arn
  port              = var.alb_port_http
  protocol          = var.alb_protocol_http

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.my-alb-client-tg.arn
  }

  tags = {
    "Name" = "${var.project}-alb_listener_http"
  }
}



