output "alb_target_grp_arn" {
  value = aws_lb_target_group.my-alb-client-tg.arn
}

output "alb_endpoint" {
  value = aws_lb.my-alb.dns_name
}

output "alb_tg_port" {
  value = aws_lb_target_group.my-alb-client-tg.port
}