# ------------- loadbalancer/variable.tf ----------------#
variable "project" {
  type        = string
  default     = "aws-terraform"
  description = "Name of the Project for which the resources are being created."
}
variable "public_sg" {
  type        = list(string)
  description = "Public Security Group"
}
variable "public_subnets" {
  type        = list(string)
  description = "List of Public Subnet Ids"
}
variable "tg_port" {
  type        = string
  description = "Target Group Port"
}
variable "vpc_id" {
  type        = string
  description = "Target VPC id"
}
variable "tg_proto" {
  type        = string
  description = "target Group Protocol"
}
variable "alb_health_check_path" {
  type        = string
  description = "ALB Healthcheck Path"
}
variable "alb_health_check_healthy_threshold" {
  type        = number
  description = "ALB Healthy Threshold"
}
variable "alb_health_check_unhealthy_threshold" {
  type        = number
  description = "ALB Un-Healthy Threshold"
}
variable "alb_health_check_timeout" {
  type        = number
  description = "ALB Healthcheck timeout"
}
variable "alb_health_check_interval" {
  type        = number
  description = "ALB Healthcheck interval"
}
variable "alb_health_check_proto" {
  type        = string
  description = "ALB Healthcheck protocol"
  default     = "HTTP"
}
variable "alb_protocol_http" {
  type        = string
  description = "ALB HTTP Listener protocol"
  default     = "HTTP"
}

variable "alb_port_http" {
  type        = number
  description = "ALB HTTP Listener Port"
  default     = 80
}